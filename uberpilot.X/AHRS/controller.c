#include "AHRS.h"
#include "../defines.h"
#include "../Comm/Comm.h"

/****************************************
Run the attitude controller at 1KHz
****************************************/

extern volatile tAHRSdata AHRSdata;
extern volatile tCmdData CmdData;
extern volatile tGains Gains;
extern volatile tMotorData MotorData;
extern _Q16 num25, num6250, num3125;

/*---------------------------------------------------------------------
  Function Name: Controller_Update
  Description:   Run the attitude controller, should execute at 1KHz
					Set motor values
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
int8_t healthcnt = 1;
void Controller_Update(void)
{
    // Quaternion attitude error
    tQuaternion qerror = qprodconj(AHRSdata.q_est, CmdData.q_cmd);

    // old gains must be multiplied by 2*255 for i2c escs and 3125 for pwm escs

    //DEBUG
    //CmdData.AttCmd = 1;
    //CmdData.throttle = 50;

    // New Quaternion Controller from Frazzoli
    _Q16 pErr = AHRSdata.p - CmdData.p;
    _Q16 qErr = AHRSdata.q - CmdData.q;
    _Q16 rErr = AHRSdata.r - CmdData.r;
    _Q16 rollCmd = mult(Gains.Kp_roll,qerror.x);
    _Q16 pitchCmd = mult(Gains.Kp_pitch,qerror.y);
    _Q16 yawCmd = mult(Gains.Kp_yaw,qerror.z);
    if (qerror.o < 0){
        rollCmd = -rollCmd;
        pitchCmd = -pitchCmd;
        yawCmd = -yawCmd;
    }

    // Increment Integrators if motor commands are being sent
    if (1 == CmdData.AttCmd || 2 == CmdData.AttCmd) {
        Gains.IntRoll += mult(rollCmd,Gains.dt);
        Gains.IntPitch += mult(pitchCmd,Gains.dt);
        Gains.IntYaw += mult(yawCmd,Gains.dt);
    }

    rollCmd += mult(Gains.Ki_roll,Gains.IntRoll) - mult(Gains.Kd_roll,pErr);
    pitchCmd += mult(Gains.Ki_pitch,Gains.IntPitch) - mult(Gains.Kd_pitch,qErr);
    yawCmd += mult(Gains.Ki_yaw,Gains.IntYaw) - mult(Gains.Kd_yaw,rErr);
    // End New Quaternion controller

    // Form motor commands
    _Q16 m1 = -pitchCmd - yawCmd;
    _Q16 m2 = -rollCmd  + yawCmd;
    _Q16 m3 =  pitchCmd - yawCmd;
    _Q16 m4 =  rollCmd  + yawCmd;

    // Get throttle value
    int16_t tmp = (int16_t) CmdData.throttle;
    _Q16 throttle = 0;
    int16toQ16(&throttle,&tmp);     // Throttle between 0.0 and 255.0 here

    if (1 == CmdData.AttCmd || 2 == CmdData.AttCmd) {

        /**** Mikrokopter ESCs - take value between 0 and 255 ****/
        // TODO - use the other avaiable command byte for greater precision
        // TODO make this a function

        uint8_t mikroData = 0;
        _Q16 motorCmd = m1 + throttle;
        Q16touint8(&mikroData,&motorCmd);      // value saturated between 1 and 255
        I2C2_WriteData(MOTOR1_ADDR, &mikroData, 1);

        motorCmd = m2 + throttle;
        Q16touint8(&mikroData,&motorCmd);
        I2C2_WriteData(MOTOR2_ADDR, &mikroData, 1);

        motorCmd = m3 + throttle;
        Q16touint8(&mikroData,&motorCmd);
        I2C2_WriteData(MOTOR3_ADDR, &mikroData, 1);

        motorCmd = m4 + throttle;
        Q16touint8(&mikroData,&motorCmd);
        I2C2_WriteData(MOTOR4_ADDR, &mikroData, 1);

        /**** PWM motors - take value between 0 and 6250 (converted to 1 <--> 2 ms PWM signal at 490Hz)*/
        int16_t pwmData = 0;
        throttle = mult(throttle,num25);  // multiply throttle by 25 to get 0 to 6250  ( assuming throttle is between 0 and 250 -- actually can be up to 255)
        motorCmd = m1 + throttle;
        _Q16sat(&motorCmd, num6250, 0);     // TODO: find lower saturation bound here so motors don't turn off
        Q16toint16(&pwmData,&motorCmd);
        SETPWM(PWM4,pwmData);

        motorCmd = m2 + throttle;
        _Q16sat(&motorCmd, num6250, 0);     // TODO: find lower saturation bound here so motors don't turn off
        Q16toint16(&pwmData,&motorCmd);
        SETPWM(PWM6,pwmData);

        motorCmd = m3 + throttle;
        _Q16sat(&motorCmd, num6250, 0);     // TODO: find lower saturation bound here so motors don't turn off
        Q16toint16(&pwmData,&motorCmd);
        SETPWM(PWM8,pwmData);

        motorCmd = m4 + throttle;
        _Q16sat(&motorCmd, num6250, 0);     // TODO: find lower saturation bound here so motors don't turn off
        Q16toint16(&pwmData,&motorCmd);
        SETPWM(PWM2,pwmData);

    } else if (0 == CmdData.AttCmd) {

        /**** Mikrokopter ESCs - take value between 0 and 255 ****/
        // TODO - use the other avaiable command byte for greater precision
        uint8_t mikroData = 0;
        I2C2_WriteData(MOTOR1_ADDR, &mikroData, 1);
        I2C2_WriteData(MOTOR2_ADDR, &mikroData, 1);
        I2C2_WriteData(MOTOR3_ADDR, &mikroData, 1);
        I2C2_WriteData(MOTOR4_ADDR, &mikroData, 1);

      	// set motor values
        SETPWM(PWM4,0);
        SETPWM(PWM6,0);
        SETPWM(PWM8,0);
        SETPWM(PWM2,0);

    }

    // read health information
    switch(healthcnt){
        case 1:
            I2C2_ReadData(MOTOR1_READ, 3);
            break;
        case 2:
            I2C2_ReadData(MOTOR2_READ, 3);
            break;
        case 3:
            I2C2_ReadData(MOTOR3_READ, 3);
            break;
        case 4:
            I2C2_ReadData(MOTOR4_READ, 3);
            healthcnt = 0;
            break;
    }
    healthcnt++;
}

/*---------------------------------------------------------------------
  Function Name: Controller_Init
  Description:   Initialize all the controller variables
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void Controller_Init(void)
{
    // ESCs
    SETPWM(PWM2,0);
    SETPWM(PWM4,0);
    SETPWM(PWM6,0);
    SETPWM(PWM8,0);

    // Servos
    SETPWM(PWM1,3125);
    SETPWM(PWM3,3125);
    SETPWM(PWM5,3125);
    SETPWM(PWM7,3125);

    CmdData.q_cmd.o = _Q16ftoi(1.0);
    CmdData.q_cmd.x = _Q16ftoi(0.0);
    CmdData.q_cmd.y = _Q16ftoi(0.0);
    CmdData.q_cmd.z = _Q16ftoi(0.0);
    CmdData.p = _Q16ftoi(0.0);
    CmdData.q = _Q16ftoi(0.0);
    CmdData.r = _Q16ftoi(0.0);
    CmdData.throttle = 0;
    CmdData.collective = 0;
    CmdData.AttCmd = 0;

    // mQxx default values
#ifdef MQXX
    Gains.Kp_roll = _Q16ftoi(600.0);
    Gains.Ki_roll = _Q16ftoi(0.0);
    Gains.Kd_roll = _Q16ftoi(200.0);

    Gains.Kp_pitch = _Q16ftoi(600.0);
    Gains.Ki_pitch = _Q16ftoi(0.0);
    Gains.Kd_pitch = _Q16ftoi(200.0);

    Gains.Kp_yaw = _Q16ftoi(0.0);
    Gains.Ki_yaw = _Q16ftoi(0.0);
    Gains.Kd_yaw = _Q16ftoi(312.5);

    Gains.maxang = _Q16ftoi(0.8);
    Gains.lowBatt = 7200; // 7.2 volts
    Gains.stream_data = 0;

#elif defined BQXX
    // BQxx default values
    Gains.Kp_roll = _Q16ftoi(100.0);
    Gains.Ki_roll = _Q16ftoi(0.0);
    Gains.Kd_roll = _Q16ftoi(30.0);

    Gains.Kp_pitch = _Q16ftoi(100.0);
    Gains.Ki_pitch = _Q16ftoi(0.0);
    Gains.Kd_pitch = _Q16ftoi(30.0);

    Gains.Kp_yaw = _Q16ftoi(0.0);
    Gains.Ki_yaw = _Q16ftoi(0.0);
    Gains.Kd_yaw = _Q16ftoi(25.5);

    Gains.maxang = _Q16ftoi(0.8);
    Gains.lowBatt = 10800; // 10.8 volts
    Gains.stream_data = 0;
#else
    Gains.Kp_roll = _Q16ftoi(0.0);
    Gains.Ki_roll = _Q16ftoi(0.0);
    Gains.Kd_roll = _Q16ftoi(0.0);

    Gains.Kp_pitch = _Q16ftoi(0.0);
    Gains.Ki_pitch = _Q16ftoi(0.0);
    Gains.Kd_pitch = _Q16ftoi(0.0);

    Gains.Kp_yaw = _Q16ftoi(0.0);
    Gains.Ki_yaw = _Q16ftoi(0.0);
    Gains.Kd_yaw = _Q16ftoi(0.0);

    Gains.maxang = _Q16ftoi(0.0);
    Gains.lowBatt = 10800; // 10.8 volts
    Gains.stream_data = 0;
#endif

    Gains.IntRoll = _Q16ftoi(0.0);
    Gains.IntPitch = _Q16ftoi(0.0);
    Gains.IntYaw = _Q16ftoi(0.0);
    Gains.dt = _Q16ftoi(1.0/1000.0);
}
//======================================================================