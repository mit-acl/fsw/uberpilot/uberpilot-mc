
#include "Comm.h"
#include "../AHRS/AHRS.h"

// Globals
extern volatile tSensorData SensorData;
extern volatile tSensorCal SensorCal;
extern volatile tLoopFlags loop;

//======================================================================
//	Variables
//----------------------------------------------------------------------

// I2C1
volatile struct 	strI2Ccmd cmdBuff1[I2C_CMD_CNT_MAX];
volatile uint16_t	_cmdWrPtr1;
volatile uint16_t	_cmdRdPtr1;
volatile uint16_t 	_cmdBufCnt1;

volatile uint16_t	_i2c1_state;
volatile uint16_t 	_i2c1_enabled =0;
volatile uint16_t 	_data_cnt1;

// I2C2
volatile struct 	strI2Ccmd cmdBuff2[I2C_CMD_CNT_MAX];
volatile uint16_t	_cmdWrPtr2;
volatile uint16_t	_cmdRdPtr2;
volatile uint16_t 	_cmdBufCnt2;

volatile uint16_t	_i2c2_state;
volatile uint16_t 	_i2c2_enabled =0;
volatile uint16_t 	_data_cnt2;

//======================================================================


//======================================================================
// 	I2C Functions
//----------------------------------------------------------------------
void I2C1_Init()
{
    // Initialize I2C
    I2C1BRG = 107; //105; // Roughly 400KHz
    I2C1CONbits.I2CSIDL = 0;
    I2C1CONbits.I2CEN = 1;
    IPC4bits.MI2C1IP = 7;
    IFS1bits.MI2C1IF = 0;
    IEC1bits.MI2C1IE = 1; // Enable interrupt

    // Initialize Variables
    _i2c1_enabled = 1;
    _cmdWrPtr1=0;
    _cmdRdPtr1=0;
    _cmdBufCnt1=0;
    _i2c1_state = I2C_STATE_IDLE;

}

void I2C2_Init()
{
    // Initialize I2C
    I2C2BRG = 107;//60;//107; //105; // Roughly 400KHz
    I2C2CONbits.I2CSIDL = 0;
    I2C2CONbits.I2CEN = 1;
    IPC12bits.MI2C2IP = 7;
    IFS3bits.MI2C2IF = 0;
    IEC3bits.MI2C2IE = 1; // Enable interrupt

    // Initialize Variables
    _i2c2_enabled = 1;
    _cmdWrPtr2=0;
    _cmdRdPtr2=0;
    _cmdBufCnt2=0;
    _i2c2_state = I2C_STATE_IDLE;

}

void I2C1_Recover()
{
    // Disable the I2C interrupt and clear flags
    I2C1CONbits.I2CEN = 0;
    IFS1bits.MI2C1IF = 0;
    IEC1bits.MI2C1IE = 0;
    I2C1STATbits.BCL = 0;

    // Float the data line and pulse the clock a few times
    // This should get the slave state machine back to normal
    // SDA1 = RG3
    // SCL1 = RG2
    _TRISG3 = 1;
    _TRISG2 = 0;

    int i,j;
    for(i=0; i < 32; i++){
        _LATG2 = 0;
        for(j=0;j<125;j++){Nop();}

        _LATG2 = 1;
        for(j=0;j<125;j++){Nop();}

    }

    // Fake an I2C stop condition
    _TRISG3 = 0;
    _LATG2 = 0;
    for(j=0;j<125;j++){Nop();}
    _LATG3 = 1;

    for(j=0;j<125;j++){Nop();}

    // Return pin directions to input
    _TRISG3 = 1;
    _TRISG2 = 1;

    // Re-initialize bus
    I2C1_Init();
}

void I2C2_Recover()
{
    // Disable the I2C interrupt and clear flags
    I2C2CONbits.I2CEN = 0;
    IFS3bits.MI2C2IF = 0;
    IEC3bits.MI2C2IE = 0;
    I2C2STATbits.BCL = 0;

    // Float the data line and pulse the clock a few times
    // This should get the slave state machine back to normal
    // SDA2 = RA3
    // SCL2 = RA2
    _TRISA3 = 1;
    _TRISA2 = 0;

    int i,j;
    for(i=0; i < 32; i++){
        _LATA2 = 0;
        for(j=0;j<125;j++){Nop();}

        _LATA2 = 1;
        for(j=0;j<125;j++){Nop();}

    }

    // Fake an I2C stop condition
    _TRISA3 = 0;
    _LATA3 = 0;
    for(j=0;j<125;j++){Nop();}
    _LATA3 = 1;

    for(j=0;j<125;j++){Nop();}

    // Return pin directions to input
    _TRISA3 = 1;
    _TRISA2 = 1;

    // Re-initialize bus
    I2C2_Init();
}
//----------------------------------------------------------------------
void I2C1_WriteData(BYTE slave, BYTE* data, BYTE len)
{
    if (len > I2C_PACKET_SIZE_MAX) return;

    // If command buffer has room
    if ( _cmdBufCnt1 < I2C_CMD_CNT_MAX )
    {
        // Disable Interrupt
        IEC1bits.MI2C1IE = 0;

        // Stuff Packet
        cmdBuff1[_cmdWrPtr1].slaveId = slave;
        cmdBuff1[_cmdWrPtr1].len = len;
        cmdBuff1[_cmdWrPtr1].write = 1;
        BYTE i;
        for(i = 0; i < len; i++)
            cmdBuff1[_cmdWrPtr1].data[i] = data[i];

        // Increment counters and pointers
        _cmdBufCnt1++;
        if(++_cmdWrPtr1==I2C_CMD_CNT_MAX){_cmdWrPtr1=0;}

        // If state idle, start the bus (will trigger interrupt)
        // Otherwise, interrupt will take care of transmission
        if (_i2c1_state == I2C_STATE_IDLE)
        {
            _i2c1_state = I2C_STATE_STARTED;
            I2C1CONbits.SEN = 1;
        }

        // Re-enable Interrupt
        IEC1bits.MI2C1IE = 1;
    }

}
//----------------------------------------------------------------------
void I2C2_WriteData(BYTE slave, BYTE* data, BYTE len)
{
    if (len > I2C_PACKET_SIZE_MAX) return;

    // If command buffer has room
    if ( _cmdBufCnt2 < I2C_CMD_CNT_MAX )
    {
        // Disable Interrupt
        IEC3bits.MI2C2IE = 0;

        // Stuff Packet
        cmdBuff2[_cmdWrPtr2].slaveId = slave;
        cmdBuff2[_cmdWrPtr2].len = len;
        cmdBuff2[_cmdWrPtr2].write = 1;
        BYTE i;
        for(i = 0; i < len; i++)
            cmdBuff2[_cmdWrPtr2].data[i] = data[i];

        // Increment counters and pointers
        _cmdBufCnt2++;
        if(++_cmdWrPtr2==I2C_CMD_CNT_MAX){_cmdWrPtr2=0;}

        // If state idle, start the bus (will trigger interrupt)
        // Otherwise, interrupt will take care of transmission
        if (_i2c2_state == I2C_STATE_IDLE)
        {
            _i2c2_state = I2C_STATE_STARTED;
            I2C2CONbits.SEN = 1;
        }

        // Re-enable Interrupt
        IEC3bits.MI2C2IE = 1;
    }

}
//----------------------------------------------------------------------
void I2C1_ReadData(BYTE slave, BYTE len)
{

    if (len > I2C_PACKET_SIZE_MAX) return;
    if (len < 1) return;

    // Disable Interrupt
    IEC1bits.MI2C1IE = 0;

    // If command buffer has room
    if ( _cmdBufCnt1 < I2C_CMD_CNT_MAX )
    {
        // Stuff Packet
        cmdBuff1[_cmdWrPtr1].slaveId = (slave | 1); // Read address
        cmdBuff1[_cmdWrPtr1].len = len;
        cmdBuff1[_cmdWrPtr1].write = 0;

        // Increment counter and pointer
        _cmdBufCnt1++;
        if(++_cmdWrPtr1==I2C_CMD_CNT_MAX){_cmdWrPtr1=0;}

        // If state idle, start the bus (will trigger interrupt)
        // Otherwise, interrupt will take care of transmission
        if (_i2c1_state == I2C_STATE_IDLE)
        {
            _i2c1_state = I2C_STATE_STARTED;
            I2C1CONbits.SEN = 1;
        }
    }

    // Re-enable Interrupt
    IEC1bits.MI2C1IE = 1;

}
//----------------------------------------------------------------------
void I2C2_ReadData(BYTE slave, BYTE len)
{
	
    if (len > I2C_PACKET_SIZE_MAX) return;
    if (len < 1) return;

    // Disable Interrupt
    IEC3bits.MI2C2IE = 0;

    // If command buffer has room
    if ( _cmdBufCnt2 < I2C_CMD_CNT_MAX )
    {
        // Stuff Packet
        cmdBuff2[_cmdWrPtr2].slaveId = (slave | 1); // Read address
        cmdBuff2[_cmdWrPtr2].len = len;
        cmdBuff2[_cmdWrPtr2].write = 0;

        // Increment counter and pointer
        _cmdBufCnt2++;
        if(++_cmdWrPtr2==I2C_CMD_CNT_MAX){_cmdWrPtr2=0;}

        // If state idle, start the bus (will trigger interrupt)
        // Otherwise, interrupt will take care of transmission
        if (_i2c2_state == I2C_STATE_IDLE)
        {
            _i2c2_state = I2C_STATE_STARTED;
            I2C2CONbits.SEN = 1;
        }
    }

    // Re-enable Interrupt
    IEC3bits.MI2C2IE = 1;

}
//----------------------------------------------------------------------


void I2C_ParseData(BYTE slave, BYTE* data, BYTE len)
{
    BYTE * ptr;
    slave -= 1;
    switch(slave){
    case GYRO_SLAVE_ADDR:
        ptr = &SensorData.gyroX;
        ptr[0] = data[1];
        ptr[1] = data[0];
        ptr[2] = data[3];
        ptr[3] = data[2];
        ptr[4] = data[5];
        ptr[5] = data[4];
        loop.GyroProp = 1; // Signal gyro propagation to start
        break;
    case ACC_SLAVE_ADDR:
        memcpy(&SensorData.accX, data, 6);
        // TODO: check for valid data -- not 0xFFFF
        break;
    case MAG_SLAVE_ADDR:
        ptr = &SensorData.magX;
        ptr[0] = data[1];
        ptr[1] = data[0];
        ptr[2] = data[3];
        ptr[3] = data[2];
        ptr[4] = data[5];
        ptr[5] = data[4];
        // TODO: check for valid data -- not 0xFFFF
        loop.AccMagCorrect = 1;
        
        break;
    case PRES_SLAVE_ADDR:
        if (len == 3) // pressure
        {
            ptr = &SensorCal.UP;
            ptr[0] = data[2];
            ptr[1] = data[1];
            ptr[2] = data[0];
            SensorCal.UP = SensorCal.UP >> (8-SensorCal.oss);
            BMP180_calc_pressure();
            loop.StartPressure = 1;
            loop.SendSensors = 1;
        } else if (len == 2) // temperature
        {
            ptr = &SensorCal.UT;
            ptr[0] = data[1];
            ptr[1] = data[0];
            loop.StartPressure = 1;
            BMP180_calc_tmperature();
        } else if (len == 22) // calibration data
        {
            ptr = &SensorCal.AC1;
            ptr[0] = data[1]; // AC1
            ptr[1] = data[0];
            ptr[2] = data[3]; // AC2
            ptr[3] = data[2];
            ptr[4] = data[5]; // AC3
            ptr[5] = data[4];
            ptr[6] = data[7]; // AC4
            ptr[7] = data[6];
            ptr[8] = data[9]; // AC5
            ptr[9] = data[8];
            ptr[10] = data[11]; // AC6
            ptr[11] = data[10];
            ptr[12] = data[13]; // B1
            ptr[13] = data[12];
            ptr[14] = data[15]; // B2
            ptr[15] = data[14];
            ptr[16] = data[17]; // MB
            ptr[17] = data[16];
            ptr[18] = data[19]; // MC
            ptr[19] = data[18];
            ptr[20] = data[21]; // MD
            ptr[21] = data[20];
        }
    case MOTOR1_ADDR:
        ptr = &SensorData.current1;
        ptr[0] = data[0];
        ptr[1] = data[1];
        ptr[2] = data[2];
        break;
    case MOTOR2_ADDR:
        ptr = &SensorData.current2;
        ptr[0] = data[0];
        ptr[1] = data[1];
        ptr[2] = data[2];
                    break;
    case MOTOR3_ADDR:
        ptr = &SensorData.current3;
        ptr[0] = data[0];
        ptr[1] = data[1];
        ptr[2] = data[2];
                    break;
    case MOTOR4_ADDR:
        ptr = &SensorData.current4;
        ptr[0] = data[0];
        ptr[1] = data[1];
        ptr[2] = data[2];
                    break;

    }
}


//======================================================================
//	Interrupts
//----------------------------------------------------------------------
void __attribute__((__interrupt__)) _MI2C1Interrupt(void)
{
    // TODO: change this??
    // Set the overflow error UART2 bit to 0
    if (U2STAbits.OERR) {
        U2STAbits.OERR = 0;
    }
    IFS1bits.U2RXIF = 0;

    // Clear Interrupt Flag
    IFS1bits.MI2C1IF = 0;

    // I2C Bus Event Occurred.  Handle with state machine
    if (I2C1STATbits.BCL)
    {
        // Todo: Something smarter here
        // Awww shiiitttttt it's a bus collision. Reset the bus.
        I2C1STATbits.BCL = 0;
        LED_BLUE = LED_ON;
        loop.I2C1Recover = 1;
        Nop();
    }else if(I2C1STATbits.IWCOL){
        LED_GREEN = LED_ON;
        Nop();
    }else
    {
        switch(_i2c1_state)
        {
            case I2C_STATE_STARTED:
                // Bus Started. Send Slave Address
                I2C1TRN = cmdBuff1[_cmdRdPtr1].slaveId;
                _i2c1_state = I2C_STATE_IDSENT;
                _data_cnt1 = 0;
                break;

            case I2C_STATE_IDSENT:
                // Check for zero length case
                if (cmdBuff1[_cmdRdPtr1].len == 0)
                {
                    // Decrement counters and increment pointers
                    if(++_cmdRdPtr1==I2C_CMD_CNT_MAX){_cmdRdPtr1=0;}
                    _cmdBufCnt1--;

                    // Stop transfer
                    _i2c1_state = I2C_STATE_STOPPED;
                    I2C1CONbits.PEN = 1;
                    break;
                }

                // Slave Address Sent.  Initiate Write or Read
                if(cmdBuff1[_cmdRdPtr1].write)
                {
                    _i2c1_state = I2C_STATE_WRITING;
                    // Note: no break here, fall through to writing case
                }
                else
                {
                    _i2c1_state = I2C_STATE_READING;

                    // Start Reception
                    I2C1CONbits.RCEN = 1;
                    break;
                }
            case I2C_STATE_WRITING:
                // Send Byte
                I2C1TRN = cmdBuff1[_cmdRdPtr1].data[_data_cnt1];

                // Check for end of packet
                if (++_data_cnt1 == cmdBuff1[_cmdRdPtr1].len)
                {
                    // Decrement counters and increment pointers
                    if(++_cmdRdPtr1==I2C_CMD_CNT_MAX){_cmdRdPtr1=0;}
                    _cmdBufCnt1--;

                    // After Interrupt, send Stop condition
                    _i2c1_state = I2C_STATE_STOP;
                }
                break;
            case I2C_STATE_READING:
                cmdBuff1[_cmdRdPtr1].data[_data_cnt1] = I2C1RCV;
                _i2c1_state = I2C_STATE_ACKED;
                // ACK unless last byte read
                if(_data_cnt1 + 1 < cmdBuff1[_cmdRdPtr1].len){
                    I2C1CONbits.ACKDT = 0;		// ACK
                }else{
                    I2C1CONbits.ACKDT = 1;		// NAK
                    I2C_ParseData(cmdBuff1[_cmdRdPtr1].slaveId, cmdBuff1[_cmdRdPtr1].data, cmdBuff1[_cmdRdPtr1].len);
                }
                I2C1CONbits.ACKEN = 1;
                break;
            case I2C_STATE_ACKED:
                if(++_data_cnt1 == cmdBuff1[_cmdRdPtr1].len)
                {
                    // End of received packet
                    // Increment pointers, update counters
                    if(++_cmdRdPtr1 == I2C_CMD_CNT_MAX){_cmdRdPtr1=0;}
                    _cmdBufCnt1--;

                    _i2c1_state = I2C_STATE_STOP; // ???

                    // Fall through to STOP the transfer
                }else{
                    // Packet still going, receive more bytes
                    _i2c1_state = I2C_STATE_READING;
                    I2C1CONbits.RCEN = 1; // Enable reception, fires interrupt
                    break;
                }
            case I2C_STATE_STOP:
                if (_cmdBufCnt1 > 0  &&  cmdBuff1[_cmdRdPtr1].write == 0){
                    // Do a repeated start if the next thing is a write
                    _i2c1_state = I2C_STATE_STARTED;
                    I2C1CONbits.SEN = 1;
                }else{
                    // Stop transfer
                    _i2c1_state = I2C_STATE_STOPPED;
                    I2C1CONbits.PEN = 1;
                }
                break;
            case I2C_STATE_STOPPED:
                if (_cmdBufCnt1 == 0)
                {
                    _i2c1_state = I2C_STATE_IDLE;
                }else{
                    _i2c1_state = I2C_STATE_STARTED;
                    I2C1CONbits.SEN = 1;
                }
                break;
            default:
                Nop();
                break;
        }
    }

}
//======================================================================
void __attribute__((__interrupt__)) _MI2C2Interrupt(void)
{
    // Set the overflow error UART2 bit to 0
    if (U2STAbits.OERR) {
        U2STAbits.OERR = 0;
    }
    IFS1bits.U2RXIF = 0;

    // Clear Interrupt Flag
    IFS3bits.MI2C2IF = 0;

    // I2C Bus Event Occurred.  Handle with state machine
    if (I2C2STATbits.BCL)
    {
        // Todo: Something smarter here
        // Awww shiiitttttt it's a bus collision. Reset the bus.
        I2C2STATbits.BCL = 0;
        LED_BLUE = LED_ON;
        loop.I2C2Recover = 1;
        Nop();
    }else if(I2C2STATbits.IWCOL){
        LED_GREEN = LED_ON;
        Nop();
    }else
    {
        switch(_i2c2_state)
        {
            case I2C_STATE_STARTED:
                // Bus Started. Send Slave Address
                I2C2TRN = cmdBuff2[_cmdRdPtr2].slaveId;
                _i2c2_state = I2C_STATE_IDSENT;
                _data_cnt2 = 0;
                break;

            case I2C_STATE_IDSENT:
                // Check for zero length case
                if (cmdBuff2[_cmdRdPtr2].len == 0)
                {
                    // Decrement counters and increment pointers
                    if(++_cmdRdPtr2==I2C_CMD_CNT_MAX){_cmdRdPtr2=0;}
                    _cmdBufCnt2--;

                    // Stop transfer
                    _i2c2_state = I2C_STATE_STOPPED;
                    I2C2CONbits.PEN = 1;
                    break;
                }

                // Slave Address Sent.  Initiate Write or Read
                if(cmdBuff2[_cmdRdPtr2].write)
                {
                    _i2c2_state = I2C_STATE_WRITING;
                    // Note: no break here, fall through to writing case
                }
                else
                {
                    _i2c2_state = I2C_STATE_READING;

                    // Start Reception
                    I2C2CONbits.RCEN = 1;
                    break;
                }
            case I2C_STATE_WRITING:
                // Send Byte
                I2C2TRN = cmdBuff2[_cmdRdPtr2].data[_data_cnt2];

                // Check for end of packet
                if (++_data_cnt2 == cmdBuff2[_cmdRdPtr2].len)
                {
                    // Decrement counters and increment pointers
                    if(++_cmdRdPtr2==I2C_CMD_CNT_MAX){_cmdRdPtr2=0;}
                    _cmdBufCnt2--;

                    // After Interrupt, send Stop condition
                    _i2c2_state = I2C_STATE_STOP;
                }
                break;
            case I2C_STATE_READING:
                cmdBuff2[_cmdRdPtr2].data[_data_cnt2] = I2C2RCV;
                _i2c2_state = I2C_STATE_ACKED;
                // ACK unless last byte read
                if(_data_cnt2 + 1 < cmdBuff2[_cmdRdPtr2].len){
                    I2C2CONbits.ACKDT = 0;		// ACK
                }else{
                    I2C2CONbits.ACKDT = 1;		// NAK
                    I2C_ParseData(cmdBuff2[_cmdRdPtr2].slaveId, cmdBuff2[_cmdRdPtr2].data, cmdBuff2[_cmdRdPtr2].len);
                }
                I2C2CONbits.ACKEN = 1;
                break;
            case I2C_STATE_ACKED:
                if(++_data_cnt2 == cmdBuff2[_cmdRdPtr2].len)
                {
                    // End of received packet
                    // Increment pointers, update counters
                    if(++_cmdRdPtr2 == I2C_CMD_CNT_MAX){_cmdRdPtr2=0;}
                    _cmdBufCnt2--;

                    _i2c2_state = I2C_STATE_STOP; // ???

                    // Fall through to STOP the transfer
                }else{
                    // Packet still going, receive more bytes
                    _i2c2_state = I2C_STATE_READING;
                    I2C2CONbits.RCEN = 1; // Enable reception, fires interrupt
                    break;
                }
            case I2C_STATE_STOP:
                if (_cmdBufCnt2 > 0  &&  cmdBuff2[_cmdRdPtr2].write == 0){
                    // Do a repeated start if the next thing is a write
                    _i2c2_state = I2C_STATE_STARTED;
                    I2C2CONbits.SEN = 1;
                }else{
                    // Stop transfer
                    _i2c2_state = I2C_STATE_STOPPED;
                    I2C2CONbits.PEN = 1;
                }
                break;
            case I2C_STATE_STOPPED:
                if (_cmdBufCnt2 == 0)
                {
                    _i2c2_state = I2C_STATE_IDLE;
                }else{
                    _i2c2_state = I2C_STATE_STARTED;
                    I2C2CONbits.SEN = 1;
                }
                break;
            default:
                Nop();
                break;
        }
    }

}
//======================================================================
