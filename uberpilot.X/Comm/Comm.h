#ifndef _COMM_H
#define _COMM_H

#include "../defines.h"
#include "../AHRS/AHRS.h"

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// UART1 and UART2
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

// Packet definitions

struct strPacket {
    BYTE id;
    BYTE len;
    BYTE data[256];
};

#define PACKETID_SENSORCAL 0x01

typedef struct sSensorCalPacket {
    int16_t gyroScale;
    int16_t accelScale;
    int16_t K_AttFilter;
    int16_t kGyroBias;
} tSensorCalPacket;

#define PACKETID_AHRS 0x02

typedef struct sAHRSpacket {
    int16_t p;
    int16_t q;
    int16_t r;
    int16_t qo_est;
    int16_t qx_est;
    int16_t qy_est;
    int16_t qz_est;
    int16_t qo_meas;
    int16_t qx_meas;
    int16_t qy_meas;
    int16_t qz_meas;
} tAHRSpacket;

#define PACKETID_CMD 0x03

typedef struct __attribute__((packed)) sCmdPacket {
    int16_t p_cmd;
    int16_t q_cmd;
    int16_t r_cmd;
    int16_t qo_cmd;
    int16_t qx_cmd;
    int16_t qy_cmd;
    int16_t qz_cmd;
    int16_t qo_meas;
    int16_t qx_meas;
    int16_t qy_meas;
    int16_t qz_meas;
    uint8_t throttle;
    int8_t collective;
    uint8_t AttCmd;
}
tCmdPacket;

#define PACKETID_GAINS 0x04

typedef struct __attribute__((packed)) sGainsPacket {
    int16_t Kp_roll;
    int16_t Kd_roll;
    int16_t Ki_roll;
    int16_t Kp_pitch;
    int16_t Kd_pitch;
    int16_t Ki_pitch;
    int16_t Kp_yaw;
    int16_t Ki_yaw;
    int16_t Kd_yaw;
    int16_t Servo1_trim;
    int16_t Servo2_trim;
    int16_t Servo3_trim;
    int16_t Servo4_trim;
    int16_t maxang;
    int16_t motorFF;
    uint16_t lowBatt;
    uint8_t stream_data;
}
tGainsPacket;

#define PACKETID_VOLTAGE 0x05

typedef struct __attribute__((packed)) sVoltagePacket {
    int16_t voltage;
}
tVoltagePacket;

#define PACKETID_LOG 0x06
typedef struct __attribute__((packed)) sLogPacket {
    int16_t p_est;
    int16_t q_est;
    int16_t r_est;
    int16_t qo_est;
    int16_t qx_est;
    int16_t qy_est;
    int16_t qz_est;
    int16_t p_cmd;
    int16_t q_cmd;
    int16_t r_cmd;
    int16_t qo_cmd;
    int16_t qx_cmd;
    int16_t qy_cmd;
    int16_t qz_cmd;
    uint8_t throttle;
    int8_t collective;
}
tLogPacket;

#define PACKETID_HEALTH 0x07

typedef struct __attribute__((packed)) sHealthPacket {
    int8_t current1;
    int8_t temp1;
    int8_t current2;
    int8_t temp2;
    int8_t current3;
    int8_t temp3;
    int8_t current4;
    int8_t temp4;
}
tHealthPacket;

#define PACKETID_SENSORS 0x08

typedef struct __attribute__((packed)) sSensorsPacket {
    int16_t gyroX;
    int16_t gyroY;
    int16_t gyroZ;
    int16_t accelX;
    int16_t accelY;
    int16_t accelZ;
    int16_t magX;
    int16_t magY;
    int16_t magZ;
    int16_t sonarZ;
}
tSensorsPacket;

#define PACKETID_ALTITUDE 0x09

typedef struct __attribute__((packed)) sAltitudePacket {
    int16_t sonarZ;
    int32_t pressure;
    int32_t temperature;
    int16_t accelX;
    int16_t accelY;
    int16_t accelZ;
}
tAltitudePacket;

#define PACKETID_SONAR 0xFA // arbitrarily chosen

// Prototypes
void UART1_PutData(unsigned char * data, unsigned int size);
void UART1_FlushTX(void);
void UART1_StuffPacket(BYTE packetId, BYTE len, BYTE* data);
void UART1_LogAHRS(void);
void UART1_SendAHRSData(void);
void UART1_SendVoltage(void);
void UART1_SendHealth(void);
void UART1_SendSensors(void);
void UART1_SendAltitude(void);
void UART1_FlushRX(void);
void UART1_Init(unsigned long int baud);

void UART2_PutData(unsigned char * data, unsigned int size);
void UART2_FlushTX(void);
void UART2_StuffPacket(BYTE packetId, BYTE len, BYTE* data);
void UART2_SendSensorData(void);
void UART2_SendAHRSData(void);
void UART2_SendVoltage(void);
void UART2_SendHealth(void);
void UART2_LogAHRS(void);
void UART2_FlushRX(void);
void UART2_Init(unsigned long int baud);

void UART_ParsePacket(struct strPacket * rxPacket);

// Defines
#define STXA 0xFF
#define STXB 0xFE
#define RCXA 0x03
#define RCXB 0x12
#define SONAR_XA 0x52 // ascii 'R'
#define SONAR_XX 0x0D // ascii 'carriage return'

#define RXSTATE_STXA			0
#define RXSTATE_STXB			1
#define RXSTATE_PACKETID		2
#define RXSTATE_LEN			3
#define RXSTATE_DATA			4
#define RXSTATE_CHKSUM			5

// scaling of parameters to send floats as ints
#define QUAT_SCALE				10000
#define ROT_SCALE				100
#define GAINS_SCALE				10

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$






// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// I2C1
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

// Prototypes
void I2C1_Init();
void I2C1_WriteData(BYTE slave, BYTE* data, BYTE len);
void I2C1_ReadData(BYTE slave, BYTE len);
void I2C1_ParseData(BYTE slave, BYTE* data);
void I2C1_Recover();

void I2C2_Init();
void I2C2_WriteData(BYTE slave, BYTE* data, BYTE len);
void I2C2_ReadData(BYTE slave, BYTE len);
void I2C_ParseData(BYTE slave, BYTE* data, BYTE len);
void I2C2_Recover();

// Defines
#ifdef V1_3
    #define GYRO_SLAVE_ADDR 0xD0
#else
    #define GYRO_SLAVE_ADDR 0xD2
#endif
#define GYRO_DATA_ADDR 0x1D

#define PRES_SLAVE_ADDR 0xEE
#define PRES_DATA_ADDR 0xF4
#define PRES_CAL 0xAA
#define PRES_TEMPERATURE 0x2E
#define PRES_PRESSURE 0x34

#define ACC_SLAVE_ADDR 0xA6
#define ACC_DATA_ADDR 0x32
#define ACC_OFFSET_ADDR 0x1E

#define MAG_SLAVE_ADDR 0x3C
#define MAG_DATA_ADDR 0x03

#define MOTOR1_ADDR		0x52
#define MOTOR1_READ		0x53
#define MOTOR2_ADDR		0x54
#define MOTOR2_READ		0x55
#define MOTOR3_ADDR		0x56
#define MOTOR3_READ		0x57
#define MOTOR4_ADDR		0x58
#define MOTOR4_READ		0x59

#define I2C_PACKET_CNT_MAX			8
#define I2C_CMD_CNT_MAX				16
#define I2C_PACKET_SIZE_MAX			32
#define I2C_STATE_IDLE				0
#define I2C_STATE_STARTED			1
#define I2C_STATE_IDSENT			2
#define I2C_STATE_WRITING			3
#define I2C_STATE_READING			4
#define I2C_STATE_ACKED				5
#define I2C_STATE_STOP				6
#define I2C_STATE_STOPPED			7

// Structs

struct strI2Ccmd {
    BYTE slaveId;
    BYTE len;
    BYTE write;
    BYTE data[I2C_PACKET_SIZE_MAX];
};

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

#endif