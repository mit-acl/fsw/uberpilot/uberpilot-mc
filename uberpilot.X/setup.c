#include "defines.h"
#include "Comm/Comm.h"
#include "AHRS/AHRS.h"

/****************************************
Setup hardware specs (timers, PWM, etc.)
****************************************/

/* globals */
extern volatile unsigned long timerCount;
extern volatile uint8_t pressureConversionCount;
extern volatile tLoopFlags loop;
extern volatile tAHRSdata AHRSdata;
extern volatile tSensorCal SensorCal;
extern volatile tSensorData SensorData;
extern _Q16 num24p5, num6250, num1p0;

/*---------------------------------------------------------------------
	Timer 1 should roll over at 1KHz
-----------------------------------------------------------------------*/
void InitTimer1( void )
{
    T1CON = 0;		/* ensure Timer 1 is in reset state */
    IFS0bits.T1IF = 0;	/* reset Timer 1 interrupt flag */
    IPC0bits.T1IP = 4;	/* set Timer1 interrupt priority level to 4 */
    IEC0bits.T1IE = 1;	/* enable Timer 1 interrupt */
    T1CONbits.TCKPS = 0b01;	// Prescaler = 8
    PR1 = 6250;		// 50MHz ticks / 8 PS = 6250 KHz / 6250 Period = 1KHz rollover
    T1CONbits.TCS = 0;	/* select internal timer clock */
    T1CONbits.TON = 1;	/* enable Timer 1 and start the count */
}

/*---------------------------------------------------------------------
	Timer 2 should roll over at 490Hz - for motor PWM Generation
-----------------------------------------------------------------------*/
void InitTimer2( void )
{
    T2CON = 0;		/* ensure Timer 2 is in reset state */
    T2CONbits.TCKPS = 0b01;	// Prescaler = 8
    PR2 = 12756;		// 50MHz ticks / 8 PS = 6250 KHz / 12756 Period = 489.97 Hz rollover
    T2CONbits.TCS = 0;	/* select internal timer clock */
    T2CONbits.TON = 1;	/* enable Timer 2 and start the count */
}

/*---------------------------------------------------------------------
	Init system clock, pin directions, etc.
-----------------------------------------------------------------------*/
void InitHardware( void )
{
    // Configure the system clock for 8MHz / 2 =4 * 50 = 200 / 2 = 100 / 2 = 50 MHz
    CLKDIVbits.PLLPRE = 0b00000; 	// Prescaler of 2
    PLLFBD = 48; 			// Multiplier of 50
    CLKDIVbits.PLLPOST = 0b00;          // Postscaler of 2

    /* 	Initialize pin directions */
    #ifdef V1_3
        _TRISB11 = _TRISB12 = _TRISB13 = _TRISB14 = 0 ;     // LED as output
    #else
        _TRISE1 = _TRISE2 = _TRISE3 = _TRISE4 = 0 ;     // LED as output
    #endif
    LED_BLUE = LED_YELLOW = LED_GREEN = LED_RED = LED_OFF ;   // Initialize LEDs as off

    // if using the electro magnet, set PWM pin 1 as output, initialize low (mag off)
#ifdef ELECTRO_MAG
    _TRISD0 = 0 ; // set pin as output
    ELECTRO_MAG_PIN = 0;    // initialize pin low
#endif

    TRISA = 0xF6FF;
    _TRISA1 = 0; 	_LATA1 = 1;	// Debug PIN - RA1, init low
}

/*---------------------------------------------------------------------
	Init the IMU
-----------------------------------------------------------------------*/
void InitIMU()
{
    BYTE data[6];

    // Configure the gyros (ITG-3200)
    data[0] = 0x16;                             // DLPF_FS register
    data[1] = 0b00011000;                       // Full scale, 8KHz sample rate
    #ifdef V1_3
        I2C1_WriteData(GYRO_SLAVE_ADDR, data, 2);   // Write to the ITG-3200
    #else
        I2C2_WriteData(GYRO_SLAVE_ADDR, data, 2);   // Write to the ITG-3200
    #endif

    #ifdef V1_3
        // Configure the accelerometer (ADXL345)
        data[0] = 0x31; // DATA_FORMAT register
        data[1] = 0x09; // Range = +- 4g, full resolution
        I2C1_WriteData(ACC_SLAVE_ADDR, data, 2); // Write to the ADXL-345
        data[0] = 0x2D; // PWR_CTL register
        data[1] = 0b00001000; // Measurement mode
        I2C1_WriteData(ACC_SLAVE_ADDR, data, 2); // Write to the ADXL-345
        data[0] = 0x2C; // BW_RATE register
	data[1] = 0x0F; // High power, 3200Hz data rate
        I2C1_WriteData(ACC_SLAVE_ADDR, data, 2); // Write to the ADXL-345

        // Configure the magnetometer (HMC5883L)
        data[0] = 0x00; // Config Reg A
        data[1] = 0b00010100; // 30Hz, normal measurement mode
        data[2] = 0b00100000; // 1.3Ga range (default)
        data[3] = 0b00000000; // Continuous mode
        I2C2_WriteData(MAG_SLAVE_ADDR, data, 4); // Write to the HMC5883 -- register automatically updates

    #endif

}

/*---------------------------------------------------------------------
  Function Name: InitPressure
  Description:   Initialize the pressure sensor
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitPressure()
{
    BYTE addr = PRES_CAL;
    SensorCal.oss = 3;  // advanced resolution
    SensorCal.pressureConversionTime = 76;  // conversion time in milliseconds
    SensorCal.temperatureConversionTime = 5;  // conversion time in milliseconds
    I2C1_WriteData(PRES_SLAVE_ADDR, &addr, 1);
    I2C1_ReadData(PRES_SLAVE_ADDR, 22);
}


/*---------------------------------------------------------------------
  Function Name: _T1Interrupt
  Description:   Timer1 Interrupt Handler, should execute at 1KHz
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void __attribute__((interrupt, auto_psv)) _T1Interrupt( void )
{
    timerCount++;

    // At 1000Hz, signal for a gyro read and run the attitude controller
    loop.ReadGyro = 1;

    // TODO: sample accelerometers faster
    // TODO: add accelerometer bias calculation
    // TODO: write accelerometer bias to bias registers.
    // At 50Hz, signal for an acc/mag read
    if ( timerCount % 20 == 1 ){
        loop.ReadAccMag = 1;
    }

    // At 20Hz, send serial data
    if ( timerCount % 50 == 0 ){
    //if ( timerCount % 200 == 0 ){
        loop.SendSerial = 1;
    }

    // At 1Hz, LED heartbeat, read temperature from pressure sensor (if v1.3+)
    if ( timerCount % 1000 == 0 ){
        loop.ToggleLED = 1;
        loop.StartTemperature = 1;
    }

    // Wait 5 ms until you can actually read the temperature or pressure.
    if (loop.StartWait == 1){
        pressureConversionCount++;
        if (pressureConversionCount > SensorCal.conversionTime){ // == 1 when 0ms have passed
            pressureConversionCount = 0;
            if (loop.StartTemperature) {
                loop.StartTemperature = 0;
                loop.ReadTemperature = 1;
            } else {
                loop.ReadPressure = 1;
                loop.CanReadPressure = 1;
            }

            loop.StartWait = 0;
        }
    }

    // At 500 Hz, read the serial buffer
    if ( timerCount % 2 == 0 ){
        loop.ReadSerial = 1;
    }

    // At 100 Hz, log data to SD card (if available
    if ( timerCount % 10 == 0 ){
            loop.LogData = 1;
    }

    IFS0bits.T1IF = 0;  /* reset timer interrupt flag	*/

}	

/*---------------------------------------------------------------------
  Function Name: InitPWM
  Description:   Start the 4 14-bit PWM generators
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitPWM(void)
{
    /* PWM Channel 1 */
#ifndef ELECTRO_MAG
    SETPWM(PWM1,0);	/* set initial PWM duty cycle */
    OC1CON = 0;		/* ensure output compare resister 1 is in a reset state */
    OC1CONbits.OCTSEL = 0;	/* choose timer 2 as the PWM timer */
    OC1CONbits.OCM = 0b110; /* PWM mode on OC1, fault pin disabled */
#endif

    /* PWM Channel 2 */
    SETPWM(PWM2,0);	/* set initial PWM duty cycle */
    OC2CON = 0;		/* ensure output compare resister 2 is in a reset state */
    OC2CONbits.OCTSEL = 0;	/* choose timer 2 as the PWM timer */
    OC2CONbits.OCM = 0b110; /* PWM mode on OC2, fault pin disabled */

    /* PWM Channel 3 */
    SETPWM(PWM3,0);	/* set initial PWM duty cycle */
    OC3CON = 0;		/* ensure output compare resister 3 is in a reset state */
    OC3CONbits.OCTSEL = 0;	/* choose timer 2 as the PWM timer */
    OC3CONbits.OCM = 0b110; /* PWM mode on OC3, fault pin disabled */

    /* PWM Channel 4 */
    SETPWM(PWM4,0);	/* set initial PWM duty cycle */
    OC4CON = 0;		/* ensure output compare resister 4 is in a reset state */
    OC4CONbits.OCTSEL = 0;	/* choose timer 2 as the PWM timer */
    OC4CONbits.OCM = 0b110; /* PWM mode on OC4, fault pin disabled */

    /* PWM Channel 5 */
    SETPWM(PWM5,0);	/* set initial PWM duty cycle */
    OC5CON = 0;		/* ensure output compare resister 5 is in a reset state */
    OC5CONbits.OCTSEL = 0;	/* choose timer 2 as the PWM timer */
    OC5CONbits.OCM = 0b110; /* PWM mode on OC5, fault pin disabled */

    /* PWM Channel 6 */
    SETPWM(PWM6,0);	/* set initial PWM duty cycle */
    OC6CON = 0;		/* ensure output compare resister 6 is in a reset state */
    OC6CONbits.OCTSEL = 0;	/* choose timer 2 as the PWM timer */
    OC6CONbits.OCM = 0b110; /* PWM mode on OC6, fault pin disabled */

    /* PWM Channel 7 */
    SETPWM(PWM7,0);	/* set initial PWM duty cycle */
    OC7CON = 0;		/* ensure output compare resister 7 is in a reset state */
    OC7CONbits.OCTSEL = 0;	/* choose timer 2 as the PWM timer */
    OC7CONbits.OCM = 0b110; /* PWM mode on OC7, fault pin disabled */

    /* PWM Channel 8 */
    SETPWM(PWM8,0);	/* set initial PWM duty cycle */
    OC8CON = 0;		/* ensure output compare resister 8 is in a reset state */
    OC8CONbits.OCTSEL = 0;	/* choose timer 2 as the PWM timer */
    OC8CONbits.OCM = 0b110; /* PWM mode on OC8, fault pin disabled */

}

/*---------------------------------------------------------------------
  Function Name: InitA2D
  Description:   Start the 12-bit A2D for voltage reading
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void InitA2D(void)
{
    AD1CON1bits.FORM   = 0b00;	// Data Output Format: integer
    AD1CON1bits.SSRC   = 0b111;	// Sample Clock Source: Auto-conversion
    AD1CON1bits.ASAM   = 1;	// ADC Sample Control: Sampling begins immediately after conversion
    AD1CON1bits.AD12B  = 1; 	// 12-bit ADC operation

    AD1CON2bits.CSCNA = 1 ;	// Scan Input Selections for CH0+ during Sample A bit
    AD1CON2bits.CHPS  = 0 ;	// Converts CH0

    AD1CON3bits.ADRC = 0 ;	// ADC Clock is derived from Systems Clock
    AD1CON3bits.ADCS = 0b000100;// ADC Conversion Clock (slowest) Tad=Tcy*(ADCS+1)= (1/40M)*12 = 0.3us (3333.3Khz)
                                // ADC Conversion Time for 12-bit Tc=14*Tad = 4.2us
    AD1CON3bits.SAMC = 1 ;	// No waiting between samples

    AD1CON2bits.VCFG = 0 ;	// use supply as reference voltage

    AD1CON1bits.ADDMABM = 1 ;       // DMA buffers are built in sequential mode
    AD1CON2bits.SMPI    = 0b1000;   // interrupt every 8th (sample,read)
    AD1CON4bits.DMABL   = 0 ;       // Each buffer contains 1 word


    AD1CSSL = 0x0000 ;
    AD1CSSH = 0x0000 ;
    AD1PCFGL= 0xFFFF ;
    AD1PCFGH= 0xFFFF ;


    #ifdef V1_5
        AD1CSSLbits.CSS6 = 1; // Enable AN6 for channel scan
        AD1PCFGLbits.PCFG6 = 0; // AN6 as Analog Input
    #elif defined V1_3
        //  Use AN28 as voltage input
        AD1CSSHbits.CSS28 = 1 ;     // Enable AN28 for channel scan
        AD1PCFGHbits.PCFG28 = 0 ;	// AN28 as Analog Input

    #else
        //  Use AN15 as voltage input
        AD1CSSLbits.CSS15 = 1 ;     // Enable AN15 for channel scan
        AD1PCFGLbits.PCFG15 = 0 ;	// AN15 as Analog Input
    #endif

    _AD1IF = 0 ;		// Clear the A/D interrupt flag bit
    _AD1IP = 1 ;		// priority 1 -- low priority
    AD1CON1bits.ADON = 1 ;	// Turn on the A/D converter
    _AD1IE = 0 ;		// Do Not Enable A/D interrupt

}

/*---------------------------------------------------------------------
  Function Name: SensorTest
  Description:   Flash leds to check if sensors are reading correctly
  Inputs:        None
  Returns:       None
-----------------------------------------------------------------------*/
void SensorTest(void)
{
    if (AHRSdata.p > num1p0){
        LED_RED = LED_ON;
    } else {
        LED_RED = LED_OFF;
    }
    if (AHRSdata.p < -num1p0){
        LED_GREEN = LED_ON;
    } else {
        LED_GREEN = LED_OFF;
    }
    if (AHRSdata.q > num1p0){
        LED_YELLOW = LED_ON;
    } else {
        LED_YELLOW = LED_OFF;
    }
    if (AHRSdata.q < -num1p0){
        LED_BLUE = LED_ON;
    } else {
        LED_BLUE = LED_OFF;
    }
    if (AHRSdata.r > num1p0){
        LED_RED = LED_ON;
        LED_GREEN = LED_ON;
    } else {
        LED_RED = LED_OFF;
        LED_GREEN = LED_OFF;
    }
    if (AHRSdata.r < -num1p0){
        LED_YELLOW = LED_ON;
        LED_BLUE = LED_ON;
    } else {
        LED_YELLOW = LED_OFF;
        LED_BLUE = LED_OFF;
    }
}