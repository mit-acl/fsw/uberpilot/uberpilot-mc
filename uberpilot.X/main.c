/*******************************
 * Written by Buddy Michini and Mark Cutler
 * acl.mit.edu
 *******************************/

#include "defines.h"
#include "Comm/Comm.h"

/******************************* 
Set device configuration values 
********************************/
_FOSCSEL(FNOSC_PRIPLL);	 			    // FastRC internal osc. with PLL speed-up
_FOSC(FCKSM_CSDCMD & OSCIOFNC_ON & POSCMD_NONE);    // use OSCIO pin for RA3
_FWDT(FWDTEN_OFF);                                  // turn off watchdog
_FICD(ICS_PGD2 & JTAGEN_OFF);			    // enable debugging by specifying correct PEGD2 ports, disable JTAG

/* globals */
extern volatile tLoopFlags loop;
extern volatile int16_t vbatt;
extern volatile tGains Gains;
extern volatile tCmdData CmdData;
extern volatile tSensorCal SensorCal;
extern volatile uint8_t pressureConversionCount;

// TODO: add check for receiving command data - land otherwise

/********************************* 
	main entry point
*********************************/
int main ( void )
{

    // Init the basic hardware
    InitCnsts();
    InitHardware();

    // Start the main 1Khz timer
    InitTimer1();
    InitTimer2();
    InitPWM();

    // Initialize A2D, I2C bus, and UARTs
    InitA2D();
    I2C1_Init();
    I2C2_Init();

    #ifdef V1_3
        //UART2_Init(SONAR_SPEED); // for maxbotic sonar
        UART2_Init(LOGGING_RC_SPEED); // for data logging and RC transmitter reading
        UART1_Init(XBEE_SPEED);  // for communication and control signals
    #else
        UART1_Init(LOGGING_RC_SPEED); // for data logging and RC transmitter reading
        UART2_Init(XBEE_SPEED);  // for communication and control signals
    #endif

    // Wait for a bit before doing rate gyro bias calibration
    // TODO: test this length of wait
    uint16_t i=0;for(i=0;i<60000;i++){Nop();}

    // turn on the leds until the bias calibration is complete
    LED_BLUE = LED_ON;
    LED_YELLOW = LED_ON;
    LED_RED = LED_ON;
    LED_GREEN = LED_ON;

    // Configure the IMU sensors
    InitIMU();

    // Initialize the pressure sensor
    #ifdef V1_3
        InitPressure();
    #endif

    // Initialize the AHRS
    AHRS_init();

    // Initialize the Controller variables
    Controller_Init();

    // TESTING: Fun with fixed point
    //_Q16 num1 = _Q16ftoi(-0.707);
    //float r1 = _itofQ16(num1);
    //_Q16 num2 = _Q16acos(num1);
    //_Q16 num3 = _Q16sin(num1);
    //float result = _itofQ16( num2 - num1 );
    //_Q16 num1 = _Q16ftoi(32768.0-2.0);
    //int16_t tmp = 0;
    //Q16toint16(&tmp,&num1);
    //Nop();

    // MAIN CONTROL LOOP: Loop forever
    while (1)
    {
        #ifdef GYRO_TEST
            SensorTest();
        #else

            // Toggle Red LED at 1Hz
            if(loop.ToggleLED){
                loop.ToggleLED = 0;
                // Toggle LED
                led_toggle(LED_RED);
                //LED_BLUE = LED_OFF;

                // Turn orange led on if battery voltage below 10.1V
                if (vbatt < Gains.lowBatt)
                        LED_YELLOW = LED_ON;
                else
                        LED_YELLOW = LED_OFF;

                #ifdef V1_3
                    UART1_SendVoltage(); 	// send voltage (data read at 20Hz in serial send loop)
                    UART1_SendHealth();		// send motor temperature and current data
                    UART1_FlushTX();
                #else
                    UART2_SendVoltage(); 	// send voltage (data read at 20Hz in serial send loop)
                    UART2_SendHealth();		// send motor temperature and current data
                    UART2_FlushTX();
                #endif

                // also start pressure sensor temperature read every second
                BYTE data[2];
                data[0] = PRES_DATA_ADDR;
                data[1] = PRES_TEMPERATURE;
                I2C2_WriteData(PRES_SLAVE_ADDR, data, 2);
                loop.StartWait = 1;
                SensorCal.conversionTime = SensorCal.temperatureConversionTime;
                loop.CanReadPressure = 0;
                pressureConversionCount = 0; // reset 5ms wait in case we are waiting for a pressure measurement right now
            }
        #endif

        // Has the I2C1 bus crashed?
        if(loop.I2C1Recover){
            loop.I2C1Recover = 0;
            I2C1_Recover();
        }
            
        // Has the I2C2 bus crashed?
        if(loop.I2C2Recover){
            loop.I2C2Recover = 0;
            I2C2_Recover();
        }

        // Gyro read
        if(loop.ReadGyro){
            loop.ReadGyro = 0;
            // Initiate i2c gyro read
            BYTE addr = GYRO_DATA_ADDR; // Address of the first data byte
            #ifdef V1_3
                I2C1_WriteData(GYRO_SLAVE_ADDR, &addr, 1);
                I2C1_ReadData(GYRO_SLAVE_ADDR, 6);

                addr = ACC_DATA_ADDR; // Address of the first data byte
                I2C1_WriteData(ACC_SLAVE_ADDR, &addr, 1);
                I2C1_ReadData(ACC_SLAVE_ADDR, 6);
            #else
                I2C2_WriteData(GYRO_SLAVE_ADDR, &addr, 1);
                I2C2_ReadData(GYRO_SLAVE_ADDR, 6);
            #endif
        }

        // Gyro propagation
        if(loop.GyroProp){
            loop.GyroProp = 0;
            // Call gyro propagation
            AHRS_GyroProp();
        }

        // Attitude control
        if(loop.AttCtl){
            loop.AttCtl = 0;
            // Call attitude control
            Controller_Update();
        }

        // Vicon gyro bias/yaw correction?
        if(loop.ViconCorrect){
            loop.ViconCorrect = 0;
            // Call correction
            AHRS_ViconCorrect();
        }

        // Send data over modem - runs at ~20Hz
        if(loop.SendSerial){
            loop.SendSerial = 0;

            // Read and low-pass filter voltage data
            vbatt = 9.0/10.0*vbatt + 1.0/10.0*(int16_t)(VBATT*VSCALE - VBIAS);

            // Send serial data
            if (Gains.stream_data)
            {
                #ifdef V1_3
                    UART1_SendAHRSData();
                    UART1_FlushTX();
                #else
                    UART2_SendAHRSData();
                    UART2_FlushTX();
                #endif
            }


            // Read in serial commands from RC transmitter
            // TODO: make this faster
            #ifdef V1_3
                UART2_FlushRX();
            #else
                UART1_FlushRX();
            #endif
            

        }

        // Read data from modem - 500 Hz
        if(loop.ReadSerial){
            loop.ReadSerial = 0;

            // Read serial data
            #ifdef V1_3
                UART1_FlushRX();
            #else
                UART2_FlushRX();
            #endif
        }

        #ifdef V1_3
            // Acc/Mag read
            if(loop.ReadAccMag){
                loop.ReadAccMag = 0;
                // Initiate Acc/Mag i2c read
//                BYTE addr = ACC_DATA_ADDR; // Address of the first data byte
//                I2C1_WriteData(ACC_SLAVE_ADDR, &addr, 1);
//                I2C1_ReadData(ACC_SLAVE_ADDR, 6);

                BYTE addr = MAG_DATA_ADDR; // Address of the first data byte
                I2C2_WriteData(MAG_SLAVE_ADDR, &addr, 1);
                I2C2_ReadData(MAG_SLAVE_ADDR, 6);
            }

            // Acc/Mag correction?
            if(loop.AccMagCorrect && (0 == CmdData.AttCmd)){
                loop.AccMagCorrect = 0;
                // Call Acc/mag correction
                //AHRS_AccMagCorrect();
            }

            // Read temperature
            if(loop.ReadTemperature){
                loop.ReadTemperature = 0;
                BYTE addr;
                addr = PRES_DATA_ADDR+0x02; // temp read address is 0xF6
                I2C1_WriteData(PRES_SLAVE_ADDR, &addr, 1);
                I2C1_ReadData(PRES_SLAVE_ADDR, 2);
            }

            // Start reading Pressure
            if(loop.StartPressure){
                loop.StartPressure = 0;
                loop.StartWait = 1;
                SensorCal.conversionTime = SensorCal.pressureConversionTime;
                BYTE data[2];
                data[0] = PRES_DATA_ADDR;
                data[1] = PRES_PRESSURE+(SensorCal.oss<<6);
                I2C1_WriteData(PRES_SLAVE_ADDR, data, 2);
            }

            // Read Pressure
            if(loop.ReadPressure && loop.CanReadPressure){
                loop.ReadPressure = 0;
                BYTE addr;
                addr = PRES_DATA_ADDR+0x02; // temp read address is 0xF6
                I2C1_WriteData(PRES_SLAVE_ADDR, &addr, 1);
                I2C1_ReadData(PRES_SLAVE_ADDR, 3);
            }


        #endif

        // Log data on UART1 to SD card - 100 Hz
        if(loop.LogData && (0 != CmdData.AttCmd)){
            loop.LogData = 0;

            // Log data on UART1 to SD card
            #ifdef V1_3
                UART2_LogAHRS();
                UART2_FlushTX();
            #else
                UART1_LogAHRS();
                UART1_FlushTX();
            #endif
            
        }

        // DEBUG!!!
        // send sensor data over serial port
        //if(loop.SendSensors){
        //    loop.SendSensors = 0;
            //UART1_SendSensors();
        //    UART1_SendAltitude();
        //    UART1_FlushTX();
        //}

    } // End while(1)
	
}


